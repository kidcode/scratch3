## 阅读材料四：Scratch 3 中的积木类型
---
<br>


Scratch 3 中的积木按不同功能及特性进行了分类，并且使用不同的颜色和外形来加以区别。只要掌握 Scratch 积木在外观上的这些规律，就能极大地方便编写程序脚本。

<br>

### 一、积木的类别

在 Scratch 3 中，根据功能分为运动、外观、声音等九个类别的积木，每个类别的颜色不同，可以根据颜色找到所需积木的类别。其中，`自制积木`类别用于制作新的积木。

<div align=center> <img src="icon/1-1-1.png"/> </div>
<div align=center> <img src="icon/1-1-2.png"/> </div>
<div align=center> <img src="icon/1-1-3.png"/> </div>
<div align=center> <img src="icon/1-1-4.png"/> </div>
<div align=center> <img src="icon/1-1-5.png"/> </div>
<div align=center> <img src="icon/1-1-6.png"/> </div>
<div align=center> <img src="icon/1-1-7.png"/> </div>
<div align=center> <img src="icon/1-1-8.png"/> </div>
<div align=center> <img src="icon/1-1-9.png"/> </div>

<a href="http://haohaodada.com/video/a21401.php" target="_blank">单击此处</a>可以观看以上内容相关的视频。

<br>

### 二、积木的五种类型

Scratch 中的积木共有五种形状，代表着不同类型的积木。

|  命令积木  |功能积木|条件积木|事件积木 |  控制积木 | 
|:-------------------:|:-------------------:|:-------------------:|:-----------------:|:---------------:|
| ![](block/1-1.png)  | ![](block/6-9.png)  | ![](block/6-1.png)  | ![](block/4-1.png)| ![](block/5-2.png)|
| ![](block/2-1.png)  | ![](block/3-9.png)  | ![](block/7-7.png)  | ![](block/4-2.png)| ![](block/5-3.png)|
| ![](block/5-1.png)  | ![](block/7-1.png)  | ![](block/7-9.png)  | ![](block/4-3.png)| ![](block/5-4.png)|

 
#### 命令积木

Scratch 中最常见的积木类型。积木上方有缺口，下方有凸起，用于拼接其它积木，使得脚本中的积木能够从上至下逐条执行。

<br>

#### 功能积木

积木形状为圆角矩形，没有缺口和凸起，因此不能单独使用。这类积木通常用来存储变量的值，可以作为其它积木的参数，也可以通过勾选积木前的复选框，在舞台上直接查看具体的数值。

<br>

#### 条件积木

积木形状为六边形，与功能积木一样不能单独使用，通常是作为判断积木的条件参数。`条件积木`只能够得到两种数值：真或假，用 `true` 或者`false` 表示。单击相关积木，能够看到该积木目前的数值。

<br>

#### 事件积木

积木形状上方为椭圆形，下方有缺口可以向下拼接积木，因此这类积木总是处于一段程序脚本的起始位置。Scratch 3 是基于`事件驱动`设计程序的，一个角色的程序脚本可以由多个事件组成，用户触发了事件以后，就会执行相应的事件积木块。

<br>

#### 控制积木

​积木上方有缺口，下方凸起，中间开口可以容纳其他积木在它的内部拼接。通常用于判断结构和循环结构，根据具体需求来编写并执行控制积木中间的积木块。

<a href="http://haohaodada.com/video/a21402.php" target="_blank">单击此处</a>可以观看以上内容相关的视频。
